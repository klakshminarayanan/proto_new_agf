module ApplicationHelper
  def process_node(queue)
    elem = queue[-1]
    if elem.length == 0
      # last element is empty array, remove it from queue and remove first element of previous element
      queue.pop
      if queue[-1]
        queue[-1].shift
      end
    end
    node = queue.dig(-1, 0)
    level = queue.length
    retval = ""
    if node
      queue.push (node.children && node.children.count > 0) ? node.children.to_a : []
      retval = "<p>"
      retval += "&nbsp;" * 7 * level;
      retval += "#{node.name}:  #{get_attributes(node)}</p>"
    end
    retval.html_safe
  end

  def get_attributes(node)
    node.try(:attributes) ? node.attributes.map { |attr_name, value| "#{attr_name}: #{value.value}" }.join(", ") : ""
  end
end

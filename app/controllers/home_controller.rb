class HomeController < ApplicationController
  def index
    gon.aws_access_key_id = $aws_access_key_id
    gon.aws_secret_access_key = $aws_secret_access_key

    @doc = Nokogiri::XML(File.open("sunspec.xml")) do |config|
      config.strict.noblanks.recover.nonet
    end
    @queue = [[@doc]]
  end

  def current
    @content = JSON.parse(File.read("ieee_1547.json"))
    render json: @content
  end
end
